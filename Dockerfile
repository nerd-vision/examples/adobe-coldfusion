FROM eaps-docker-coldfusion.bintray.io/cf/coldfusion:latest as base

FROM python:3 as builder

COPY --from=base /opt/coldfusion/cfusion/bin/jvm.config /opt/jvm.config
COPY update_java_opts.py /opt/update_java_opts.py

ENV API_KEY="your api key here"
ENV JAVA_OPTS="-javaagent:/opt/nerdvision/nerdvision.jar=api.key=${API_KEY}"

RUN /opt/update_java_opts.py -f /opt/jvm.config

FROM eaps-docker-coldfusion.bintray.io/cf/coldfusion:latest

RUN mkdir --parents /opt/nerdvision

COPY --from=builder /opt/jvm.config /opt/coldfusion/cfusion/bin/jvm.config
ADD "https://repository.sonatype.org/service/local/artifact/maven/redirect?r=central-proxy&g=com.nerdvision&a=agent&v=LATEST" /opt/nerdvision/nerdvision.jar

RUN chown -R cfuser:bin /opt/nerdvision

ENV acceptEULA=YES
ENV password=ColdFusion123
