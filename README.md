# Adobe Coldfusion Example

This is a quick example of using nerd.vision with [Adobe Coldfusion](https://coldfusion.adobe.com/).

## Install

To install nerd.vision you just need to add the nerd.vision agent to the jvm config. See our docs on [installing ColdFusion](https://docs.nerd.vision/coldfusion/install/) for more info.

## Run app
First set the APIKEY for nv in the Dockerfile.

To run this app use this command:

```bash
docker-compose up
```
