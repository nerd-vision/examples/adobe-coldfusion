#!/usr/bin/env python3

import argparse
import os


def main():
    parser = argparse.ArgumentParser(description="Patch jvm.config to insert JAVA_OPTS the file.")
    parser.add_argument("-f", "--file", type=str, default="", required=True,
                        help="The file to patch.")
    parser.add_argument('-p', '--pretend', action="store_true", default=False, help="Pretend only.")
    parser.add_argument("-V", "--verbose", help='Use verbose logging for this script.',
                        default=os.environ.get("VERBOSE", False), action='store_true', required=False)

    args = parser.parse_args()

    try:
        opts = os.environ['JAVA_OPTS']
    except Exception:
        opts = ""

    if args.verbose:
        print("Updating JAVA_OPTS with: ", opts)

    out_lines = []
    with open(args.file, "r") as ins:
        lines = ins.readlines()
        for line in lines:
            if line.find("java.args=") == 0:
                out_lines.append("java.args=%s %s" % (os.getenv("JAVA_OPTS", ""), line[10:]))
            else:
                out_lines.append(line)

    with open(args.file, 'w') as out:
        out.writelines(out_lines)


if __name__ == "__main__":
    main()
